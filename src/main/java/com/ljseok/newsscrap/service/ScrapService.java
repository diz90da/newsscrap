package com.ljseok.newsscrap.service;

import com.ljseok.newsscrap.model.NewsItem;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

@Service
public class ScrapService {
    private Document getFullHtml() throws IOException {
        String url = "http://www.astronomer.rocks/"; // 접속할 주소
        Connection connection = Jsoup.connect(url);

        Document document = connection.get();

        return document;
    }

    private List<Element> parseHtml(Document document) {
        Elements elements = document.getElementsByClass("auto-article");

        List<Element> tempResult = new LinkedList<>();

        for (Element item : elements) {
            Elements lis = item.getElementsByTag("li");
            for (Element item2 : lis) {
                tempResult.add(item2);
            }
        }

        return tempResult;
    }

    private List<NewsItem> makeResult(List<Element> list) {
        List<NewsItem> result = new LinkedList<>();

        for (Element item : list) {
            Elements checkContents = item.getElementsByClass("flow-hidden");
            if (checkContents.size() == 0) {
                Elements checkHiddenBanner = item.getElementsByClass("auto-fontB");
                if (checkHiddenBanner.size() > 0) {
                    String title = item.getElementsByClass("auto-titles").get(0).getElementsByTag("strong").get(0).text();
                    String content = item.getElementsByClass("auto-fontB").get(0).text();

                    NewsItem addItem = new NewsItem();
                    addItem.setTitle(title);
                    addItem.setContent(content);

                    result.add(addItem);
                }
            }
        }

        return result;
    }

    public List<NewsItem> run() throws IOException {
        Document document = getFullHtml();
        List<Element> elements = parseHtml(document);
        List<NewsItem> result = makeResult(elements);

        return result;
    }
}

