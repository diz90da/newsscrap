package com.ljseok.newsscrap.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MoneyChangeRequest {
    private Integer money;

}
